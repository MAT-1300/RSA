RSA, alternative A
===

# Requirements 
Golang version 1.5 or above.

# How to run
Execute the <build> file on a Linux machine
```
$ chmod +x ./build
$ ./build
```
Choose ```i```nstall and ```r```un to install and run the application.

On Windows you need to run the individual go-commands to install and run the application:
```
# go install ./src/main
# ./bin/main
```
