package common

/*
Common: Common functions used around...
*/

import (
    "log"
    "strconv"
)

func IntLen(i int64) int {
    a := strconv.FormatInt(i, 10)
    return len(a)
}

func errMsg(err error) string {
    if err == nil {
        return ""
    }
    return err.Error()
}

func IsError(err error, msg string) bool {
    if err != nil {
        log.Print(msg + ": " + errMsg(err))
        return true
    }
    return false
}

func Fatal(err error, msg string) {
    if err != nil {
        log.Fatal(msg + ": " + errMsg(err))
    }
}

func Panic(err error, msg string) {
    if err != nil {
        panic(msg + ": " + errMsg(err))
    }
}
